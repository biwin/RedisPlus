package com.maxbill.fxui.util;

/**
 * UI常量类
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public class CommonConstant {

    public static final String DATA_TREE_TYPE_C = "C";
    public static final String DATA_TREE_TYPE_S = "S";
    public static final String DATA_TREE_TYPE_D = "D";


    public static final String APP_NAME = "RedisPlus";
    public static final String APP_ICON = "/images/apps-icon-01.png";
    public static final String APP_LOGO = "/images/apps-icon-02.png";
    public static final String APP_VERSION = "v4.0.0";

    public static final String MENU_ICON_01 = "/images/menu-icon-01.png";
    public static final String MENU_ICON_02 = "/images/menu-icon-02.png";
    public static final String MENU_ICON_03 = "/images/menu-icon-03.png";
    public static final String MENU_ICON_04 = "/images/menu-icon-04.png";
    public static final String MENU_ICON_05 = "/images/menu-icon-05.png";
    public static final String MENU_ICON_06 = "/images/menu-icon-06.png";
    public static final String MENU_ICON_07 = "/images/menu-icon-07.png";

    public static final String FOOT_ICON_01 = "/images/foot-icon-01.png";
    public static final String FOOT_ICON_02 = "/images/foot-icon-02.png";


    public static final String BODY_TREE_01 = "/images/body-tree-01.png";
    public static final String BODY_TREE_02 = "/images/body-tree-02.png";

    public static final String LEFT_TREE_01 = "/images/left-tree-01.png";
    public static final String LEFT_TREE_02 = "/images/left-tree-02.png";
    public static final String LEFT_TREE_03 = "/images/left-tree-03.png";
    public static final String LEFT_TREE_04 = "/images/left-tree-04.png";

    public static final String DATA_JSON_CONF = "/uidata/conf.json";
    public static final String DATA_JSON_INFO = "/uidata/info.json";


    public static final String STYLE_MAIN = "/styles/win-main.css";
    public static final String STYLE_BODY = "/styles/win-body.css";
    public static final String STYLE_INFO = "/styles/win-info.css";
    public static final String STYLE_WARN = "/styles/win-warn.css";
    public static final String STYLE_LOGS = "/styles/win-logs.css";
    public static final String STYLE_HEAD = "/styles/win-head.css";
    public static final String STYLE_LEFT = "/styles/win-left.css";
    public static final String STYLE_FOOT = "/styles/win-foot.css";
    public static final String STYLE_SBAR = "/styles/win-sbar.css";

    public static final String TEXT_TAB_DATA = "服务数据";
    public static final String TEXT_TAB_INFO = "服务信息";
    public static final String TEXT_TAB_CONF = "服务配置";
    public static final String TEXT_TAB_WARN = "服务监控";
    public static final String TEXT_TAB_LOGS = "日志信息";

    public static final String MESSAGE_STATUE_NO = "未连接到服务...";
    public static final String MESSAGE_LEFT_SINGLES = "单机服务";
    public static final String MESSAGE_LEFT_CLUSTER = "集群服务";

    public static final String MESSAGE_CONNECT_01 = "SSH认证失败";
    public static final String MESSAGE_CONNECT_02 = "未知的主机";
    public static final String MESSAGE_CONNECT_03 = "网络不可达";
    public static final String MESSAGE_CONNECT_04 = "密码不正确";
    public static final String MESSAGE_CONNECT_05 = "请求服务超时";
    public static final String MESSAGE_CONNECT_06 = "服务拒绝连接";
    public static final String MESSAGE_CONNECT_07 = "密码不能为空";
    public static final String MESSAGE_CONNECT_08 = "请检查服务状态";
    public static final String MESSAGE_CONNECT_09 = "打开SSH连接超时";
    public static final String MESSAGE_CONNECT_10 = "打开连接失败";


}
